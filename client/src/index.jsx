import React from 'react';
import ReactDOM from 'react-dom';
import { HomeComponent } from './pages/home.component';
import './index.scss';
import SocketService from './shared/services/socket.service';

ReactDOM.render(<HomeComponent />, document.getElementById('root'));

SocketService.connect();