import SocketService from '../../../src/shared/services/socket.service.js';
import SocketIoClient from 'socket.io-client';

describe('Ma suite de test', () => {
	it('Mon test', () => {
		const mockSocketIo = SocketIoClient.io = jest.fn();
		SocketService.connect();
		expect(mockSocketIo).toHaveBeenCalledTimes(1);
	});
});
